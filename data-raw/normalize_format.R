#' normalize_coldate_fmt_from
#'
#' @param modalites modalites
#'
#' @return col
#' @noRd
normalize_coldate_fmt_from <- function(modalites)
{
  dplyr::case_when(
    modalites == "aaaa"                                        ~ '%Y',
    modalites == "jj/ mm/ aaaa"                                ~ '%d/%m/%Y',
    modalites == "mm/ aaaa"                                    ~ '%m/%Y',
    modalites == "mm/ aaaa \n01/1900 : diagnostic non réalisé" ~ '%m/%Y',
    TRUE ~ ''
  )
}




#' normalize_coltype_from
#'
#' @param format format
#'
#' @return col
#' 
#' @noRd
normalize_coltype_from <- function(format){

    dplyr::case_when(
      format %in% c('Date','DATE')         ~  'date',
      format == 'Numérique'                ~  'integer',
      stringr::str_starts(format, 'Alpha') ~  'character',
      TRUE                                 ~  'character'
    )

}

##' 
##'
##' 
##' @title normalize_name
##' 
##' @param attribname attribname
##' @param format format
##'
##' @return col
##' 
#' @noRd
normalize_name <- function(attribname,format)
{
  dplyr::case_when(
    format %in% c('Date','DATE')         ~  glue::glue('dt.{attribname}'),
    TRUE                                 ~  attribname
  )
}



as_annee_int <- function(x)
{
  as.integer(format(x,format="%Y"))
}

as_anneemois_char <- function(x)
{
  format(x,format="%m/%Y")
}



as_dma_char <- function(x)
{
  format(x,format="%d/%m/%Y")
}



format_column_following_specs <- function(.x, tcol, readrspecs){
  fn_name <- get_colspec_fmtf(tcol,readrspecs)
  fn <- get(fn_name)
  fn(.x)  
}



strip_fmtdt <- function(x)
{
  stringr::str_sub(x,start=stringr::str_length('fmt.dt.')+1)  
}


